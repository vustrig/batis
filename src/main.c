#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

#include "vector.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define M_PI 3.14159265f

typedef enum {
	OBJECT_PLANE,
	OBJECT_SPHERE,
} ObjectType;

typedef struct {
	vec n;
	float d;
} Plane;

typedef struct {
	vec p;
	float r;
} Sphere;

typedef struct {
	ObjectType type;
	uint32_t material;
	union {
		Plane plane;
		Sphere sphere;
	};
} Object;

typedef struct {
	vec color;
	vec emission;
	float roughness;
	float refractive;
} Material;

typedef struct {
	vec o;
	vec d;
} Ray;

typedef struct {
	vec origin;
	vec target;
	vec up;
	uint32_t width;
	uint32_t height;
	uint32_t depth;
	uint32_t samples;
	uint32_t seed;
	float fov;
	float exposure;
} Camera;

typedef struct {
	Object *objects;
	Material *materials;
	uint32_t object_count;
	uint32_t material_count;
} World;

bool IntersectRayPlane(Plane *plane, Ray *ray, vec *normal_out, float *t_out) {
	float epsilon = 0.001f;
	float align = vec_inner(plane->n, ray->d);
	if (fabs(align) < epsilon) return false;

	vec relative = vec_sub_vv(ray->o, vec_mul_vs(plane->n, plane->d));
	float dist = vec_inner(relative, plane->n);
	float t = -dist / align;
	if (t < 0) return false;

	*normal_out = plane->n;
	*t_out = t;
	return true;
}

bool IntersectRaySphere(Sphere *sphere, Ray *ray, vec *normal_out, float *t_out) {
	vec m = vec_sub_vv(ray->o, sphere->p);
	float b = vec_inner(m, ray->d);
	float c = vec_inner(m, m) - sphere->r * sphere->r;
	if (c > 0.f && b > 0.f) return false;
	float delta = b * b - c;
	if (delta <= 0.0f) return false;
	float sd = sqrtf(delta);
	float t = - b - sd;
	if (t < 0) t = -b + sd;
	if (t < 0) return false;
	*t_out = t;
	vec p = vec_add_vv(ray->o, vec_mul_vs(ray->d, t));
	vec n = vec_sub_vv(p, sphere->p);
	*normal_out = vec_normalize(n);
	return true;
}

bool IntersectRay(Object *object, Ray *ray, vec *normal_out, float *t_out) {
	switch(object->type) {
		case OBJECT_PLANE:
			return IntersectRayPlane(&object->plane, ray, normal_out, t_out);
		case OBJECT_SPHERE:
			return IntersectRaySphere(&object->sphere, ray, normal_out, t_out);
	}
	return false;
}

static float Halton(uint32_t index, uint32_t base) {
	assert(index != 0xffffffff);
	index += 1; //indices start at 0
	float f = 1;
	float r = 0;
	while (index > 0) {
		f = f/base;
		r = r + f * (index % base);
		index = index / base;
	}
	return r;
}

static float Fresnel(vec ray, vec normal, float n1, float n2) {
	float r0 = (n1 - n2) / (n1 + n2);
	r0 *= r0;
	float ic = 1 + vec_inner(ray, normal);
	return r0 + (1 - r0) * ic * ic * ic * ic * ic;
}

static uint8_t* Raytrace(Camera camera, World world) {
	uint8_t *data = malloc(camera.width * camera.height * 3 * sizeof(*data));
	if (!data) return 0;

	RNG rng;
	rng_init(&rng, camera.seed);

	float fov = camera.fov / 180.f * M_PI;
	float screen_width = 1.f;
	float aspect = (float)camera.width / (float)camera.height;
	float screen_height = screen_width / aspect;
	float screen_dist = (screen_width / 2.f) / tanf(fov / 2.f);

	vec camera_direction = vec_normalize(vec_sub_vv(camera.target, camera.origin));

	vec screen_right = vec_normalize(vec_cross(camera_direction, camera.up));
	vec screen_up = vec_cross(screen_right, camera_direction);
	vec screen_center = vec_add_vv(camera.origin, vec_mul_vs(camera_direction, screen_dist));
	vec screen_upper_left = screen_center;
	screen_upper_left = vec_sub_vv(screen_upper_left, vec_mul_vs(screen_right, screen_width / 2.f));
	screen_upper_left = vec_add_vv(screen_upper_left, vec_mul_vs(screen_up, screen_height / 2.f));

	float horizontal_step = screen_width / (camera.width - 1);
	float vertical_step = screen_height / (camera.height - 1);

	for (uint32_t y = 0; y < camera.height; y++) {
		for (uint32_t x = 0; x < camera.width; x++) {
			vec point = screen_upper_left;
			point = vec_add_vv(point, vec_mul_vs(screen_right, x * horizontal_step));
			point = vec_sub_vv(point, vec_mul_vs(screen_up, y * vertical_step));

			vec color = {0, 0, 0};

			for (uint32_t sample = 0; sample < camera.samples; sample++) {

				vec sample_point = point;
				float horizontal_offset = (Halton(sample, 2) - 0.5f) * horizontal_step;
				float vertical_offset   = (Halton(sample, 3) - 0.5f) * vertical_step;
				sample_point = vec_add_vv(sample_point, vec_mul_vs(screen_right, horizontal_offset));
				sample_point = vec_add_vv(sample_point, vec_mul_vs(screen_up, vertical_offset));

				Ray r = {
					.o = camera.origin,
					.d = vec_normalize(vec_sub_vv(sample_point, camera.origin)),
				};

				vec sample_color = {0, 0, 0};
				vec absorb = {1, 1, 1};

				uint32_t last = 0;
				vec last_normal;

				for (uint32_t pass = 0; pass < camera.depth; pass++) {
					float t = INFINITY;
					uint32_t object = 0;
					bool found = false;
					vec normal;
					for (uint32_t i = 0; i < world.object_count; i++) {
						if (pass && i == last) continue;
						vec n;
						float tt;
						if (!IntersectRay(&world.objects[i], &r, &n, &tt) || tt > t) continue;
						found = true;
						t = tt;
						object = i;
						normal = n;
					}

					last = object;

					uint32_t material_index = world.objects[object].material;

					if (found) {
						if (vec_inner(r.d, normal) > 0) break;
						if (absorb.x == 0 && absorb.y == 0 && absorb.z == 0) break;
						sample_color = vec_add_vv(sample_color, vec_hadamard(absorb, world.materials[material_index].emission));
						r.o = vec_add_vv(r.o, vec_mul_vs(r.d, t));
						vec dir_random = vec_transform_to(vec_random_normal(&rng), normal);
						vec norm = vec_slerp(normal, dir_random, world.materials[material_index].roughness);
						if (vec_inner(norm, r.d) > 0.f) norm = vec_mul_vs(norm, -1.f);
						float spec_chance = Fresnel(r.d, norm, 1.0, world.materials[material_index].refractive);
						float random = rng_next_real(&rng);
						if (random < spec_chance) { // specular
							r.d = vec_reflect(r.d, norm);
						} else { //diffuse
							r.d = dir_random;
							absorb = vec_hadamard(absorb, world.materials[material_index].color);
						}
						if (pass) {
							float lambert = vec_inner(normal, r.d);
							absorb = vec_mul_vs(absorb, lambert);
						}
						last_normal = normal;
					} else {
						sample_color = vec_add_vv(sample_color, vec_hadamard(absorb, world.materials[0].emission));
						break;
					}
				}
				color = vec_add_vv(color, vec_div_vs(sample_color, camera.samples));
			}

			size_t offset = (x + y * camera.width) * 3;
			color = vec_mul_vs(color, camera.exposure);
			data[offset + 0] = linear_to_sRGB(color.x);
			data[offset + 1] = linear_to_sRGB(color.y);
			data[offset + 2] = linear_to_sRGB(color.z);
		}
		if (!(y % 0x10))
			fprintf(stderr, "%d/%d\n", y, camera.height);
	}
	return data;
}

int main(int argc, char **argv) {
	if (argc < 2) {
		printf("ERROR: Output filename required.\n");
		return 1;
	}

	Camera camera = {
		.origin = {0, 1, 7},
		.target = {0, 0, 0},
		.up = {0, 1, 0},
		.width = 1920,
		.height = 1080,
		.fov = 90.f,
		.depth = 16,
		.samples = 16,
		.seed = 123,
		.exposure = 0.1,
	};

	World world;
	world.object_count = 9;
	world.material_count = 6;
	world.objects = malloc(world.object_count * sizeof(Object));
	world.materials = malloc(world.material_count * sizeof(Material));

	world.objects[0].type = OBJECT_PLANE;
	world.objects[0].material = 1;
	world.objects[0].plane.n = vec_v3(0, 1, 0);
	world.objects[0].plane.d = 0;

	world.objects[1].type = OBJECT_SPHERE;
	world.objects[1].material = 2;
	world.objects[1].sphere.p = vec_v3(0, 0, 0);
	world.objects[1].sphere.r = 2;

	world.objects[2].type = OBJECT_SPHERE;
	world.objects[2].material = 3;
	world.objects[2].sphere.p = vec_v3(4, 0.5, 2);
	world.objects[2].sphere.r = 0.7;

	world.objects[3].type = OBJECT_SPHERE;
	world.objects[3].material = 5;
	world.objects[3].sphere.p = vec_v3(-2, 1.3, 3);
	world.objects[3].sphere.r = 0.7;

	world.objects[4].type = OBJECT_SPHERE;
	world.objects[4].material = 4;
	world.objects[4].sphere.p = vec_v3(3, 0.3, 1);
	world.objects[4].sphere.r = 0.2;

	world.objects[5].type = OBJECT_SPHERE;
	world.objects[5].material = 1;
	world.objects[5].sphere.p = vec_v3(-1.5, 0.7, 3.23);
	world.objects[5].sphere.r = 0.15;

	world.objects[6].type = OBJECT_SPHERE;
	world.objects[6].material = 1;
	world.objects[6].sphere.p = vec_v3(-1.54, 0.4, 3.3);
	world.objects[6].sphere.r = 0.2;

	world.objects[7].type = OBJECT_SPHERE;
	world.objects[7].material = 1;
	world.objects[7].sphere.p = vec_v3(-1.52, 0.0, 3.1);
	world.objects[7].sphere.r = 0.3;

	world.objects[8].type = OBJECT_SPHERE;
	world.objects[8].material = 4;
	world.objects[8].sphere.p = vec_v3(3, 0.3, 1);
	world.objects[8].sphere.r = 0.2;

	world.materials[0].color      = vec_v3(0.0f, 0.0f, 0.0f);
	world.materials[0].emission   = vec_v3(4.3f, 4.3f, 6.0f);
	world.materials[0].roughness  = 1.0f;
	world.materials[0].refractive = 1.4f;

	world.materials[1].color      = vec_v3(0.7f, 0.7f, 0.2f);
	world.materials[1].emission   = vec_v3(0.0f, 0.0f, 0.0f);
	world.materials[1].roughness  = 0.4f;
	world.materials[1].refractive = 1.4f;

	world.materials[2].color      = vec_v3(0.5f, 0.3f, 0.2f);
	world.materials[2].emission   = vec_v3(0.0f, 0.0f, 0.0f);
	world.materials[2].roughness  = 0.01f;
	world.materials[2].refractive = 1.4f;

	world.materials[3].color      = vec_v3(0.2f, 0.7f, 0.2f);
	world.materials[3].emission   = vec_v3(0.0f, 0.0f, 0.0f);
	world.materials[3].roughness  = 0.3f;
	world.materials[3].refractive = 1.4f;

	world.materials[4].color      = vec_v3(0.02f, 0.02f, 0.02f);
	world.materials[4].emission   = vec_v3(40.0f, 9.f, 60.0f);
	world.materials[4].roughness  = 0.9f;
	world.materials[4].refractive = 1.4f;

	world.materials[5].color      = vec_v3(0.02f, 0.02f, 0.02f);
	world.materials[5].emission   = vec_v3(10.0f, 90.f, 3.0f);
	world.materials[5].roughness  = 0.9f;
	world.materials[5].refractive = 1.4f;

	RNG rng;
	rng_init(&rng, camera.seed);

	/*
	for (int i = 0; i < 16; i++) {
		vec temp = vec_random_normal(&rng);
		//printf("% f % f % f\n", temp.x, temp.y, temp.z);
	}
	*/

	uint8_t *data = Raytrace(camera, world);

	stbi_write_png(argv[1], camera.width, camera.height, 3, data, 0);
}

