#ifndef VECTOR_H
#define VECTOR_H

#define M_PI 3.14159265f

#include <math.h>
#include "random.h"

typedef struct {
	float x, y, z;
} vec;

static vec vec_v3(float x, float y, float z);

static float vec_inner(vec lhs, vec rhs);
static vec   vec_cross(vec lhs, vec rhs);
static vec   vec_hadamard(vec lhs, vec rhs);
static vec   vec_normalize(vec v);

static vec vec_add_vv(vec lhs, vec rhs);
static vec vec_sub_vv(vec lhs, vec rhs);

static vec vec_add_vs(vec lhs, float rhs);
static vec vec_sub_vs(vec lhs, float rhs);
static vec vec_mul_vs(vec lhs, float rhs);
static vec vec_div_vs(vec lhs, float rhs);

static vec vec_reflect(vec dir, vec normal);
static vec vec_slerp(vec lhs, vec rhs, float t);

static vec vec_random_normal(RNG *rng);
static vec vec_transform_to(vec from, vec to);

static void vec_sRGB(vec color, uint8_t *r, uint8_t *g, uint8_t *b);

static vec vec_v3(float x, float y, float z) {
	vec ret = {x, y, z};
	return ret;
}

static vec vec_add_vv(vec lhs, vec rhs) {
	vec ret = {
		.x = lhs.x + rhs.x,
		.y = lhs.y + rhs.y,
		.z = lhs.z + rhs.z,
	};
	return ret;
}

static vec vec_sub_vv(vec lhs, vec rhs) {
	vec ret = {
		.x = lhs.x - rhs.x,
		.y = lhs.y - rhs.y,
		.z = lhs.z - rhs.z,
	};
	return ret;
}

static vec vec_add_vs(vec lhs, float rhs) {
	vec ret = {
		.x = lhs.x + rhs,
		.y = lhs.y + rhs,
		.z = lhs.z + rhs,
	};
	return ret;
}

static vec vec_sub_vs(vec lhs, float rhs) {
	vec ret = {
		.x = lhs.x - rhs,
		.y = lhs.y - rhs,
		.z = lhs.z - rhs,
	};
	return ret;
}

static vec vec_mul_vs(vec lhs, float rhs) {
	vec ret = {
		.x = lhs.x * rhs,
		.y = lhs.y * rhs,
		.z = lhs.z * rhs,
	};
	return ret;
}

static vec vec_div_vs(vec lhs, float rhs) {
	vec ret = {
		.x = lhs.x / rhs,
		.y = lhs.y / rhs,
		.z = lhs.z / rhs,
	};
	return ret;
}

static float vec_inner(vec lhs, vec rhs) {
	float ret = 0;
	ret += lhs.x * rhs.x;
	ret += lhs.y * rhs.y;
	ret += lhs.z * rhs.z;
	return ret;
}

static vec vec_cross(vec lhs, vec rhs) {
	vec ret = {
		.x = lhs.y * rhs.z - lhs.z * rhs.y,
		.y = lhs.z * rhs.x - lhs.x * rhs.z,
		.z = lhs.x * rhs.y - lhs.y * rhs.x,
	};
	return ret;
}

static vec vec_hadamard(vec lhs, vec rhs) {
	vec ret = {
		.x = lhs.x * rhs.x,
		.y = lhs.y * rhs.y,
		.z = lhs.z * rhs.z,
	};
	return ret;
}

static vec vec_normalize(vec v) {
	float len2 = vec_inner(v, v);
	float len = sqrtf(len2);
	return vec_div_vs(v, len);
}

static uint8_t linear_to_sRGB(float lin) {
	float threshold = 0.0031308f;
	float srgb;
	if (lin <= threshold) {
		srgb = 12.92 * lin;
	} else {
		srgb = 1.055 * powf(lin, 1/2.4f);
	}
	if (srgb < 0) srgb = 0;
	if (srgb > 1) srgb = 1;
	srgb *= 256;
	int32_t integer = (int32_t)srgb;
	if (integer < 0x00) integer = 0x00;
	if (integer > 0xff) integer = 0xff;
	return (uint8_t) integer;
}

static vec vec_reflect(vec dir, vec normal) {
	float d = vec_inner(dir, normal);
	return vec_add_vv(dir, vec_mul_vs(normal, -2 * d));
}

static vec vec_slerp(vec lhs, vec rhs, float t) {
/*
	vec aaa = vec_mul_vs(lhs, 1-t);
	vec bbb = vec_mul_vs(rhs, t);
	vec ccc = vec_add_vv(aaa, bbb);
	return vec_normalize(ccc);
*/


	float c = vec_inner(lhs, rhs);
	if (c > 1.f) c = 1.f;
	float omega = acosf(c);
	float s = sinf(omega);
	if (s < 0.0001f) return lhs;
	float s1 = sinf((1 - t) * omega);
	float s2 = sinf(t * omega);
	return vec_add_vv(vec_mul_vs(lhs, s1 / s), vec_mul_vs(rhs, s2 / s));
}

static vec vec_random_normal(RNG *rng) {
	float angle = rng_next_real(rng);
	float offset = rng_next_real(rng);

	angle *= 2.f * M_PI;
	offset = acos(offset);

	float x = sin(offset) * sin(angle);
	float y = cos(offset);
	float z = sin(offset) * cos(angle);

	return vec_v3(x, y, z);
}

static vec vec_transform_to(vec from, vec to) {
	vec t0 = vec_cross(vec_v3(1, 0, 0), to);
	//if (vec_inner(t0, t0) < 0.1f) t0 = vec_cross(vec_v3(0, 0, 1), to);
	t0 = vec_normalize(t0);
	vec t1 = vec_cross(t0, to);
	t1 = vec_normalize(t1);

	vec ret;

	ret.x = t0.x * from.x + to.x * from.y + t1.x * from.z;
	ret.y = t0.y * from.x + to.y * from.y + t1.y * from.z;
	ret.z = t0.z * from.x + to.z * from.y + t1.z * from.z;

	return ret;
}

#endif // VECTOR_H
