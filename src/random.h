#ifndef RANDOM_H
#define RANDOM_H

#include <stdint.h>

typedef struct {
	uint32_t state[4];
} RNG;

static void rng_init(RNG *rng, uint32_t seed);
static uint32_t rng_next(RNG *rng);
static float rng_next_real(RNG *rng);

static void rng_init(RNG *rng, uint32_t seed) {
	rng->state[0] = seed;
	rng->state[1] = seed ^ 0xAAAAAAAA;
	rng->state[2] = seed ^ 0x55555555;
	rng->state[3] = ~seed;
}

static uint32_t rng_next(RNG *rng) {
	uint32_t s, t;
	t = rng->state[3];
	t ^= t << 11;
	t ^= t >> 8;
	rng->state[3] = rng->state[2];
	rng->state[2] = rng->state[1];
	rng->state[1] = rng->state[0];
	s = rng->state[0];
	t ^= s;
	t ^= s >> 19;
	rng->state[0] = t;
	return t;
}

static float rng_next_real(RNG *rng) {
	uint32_t number = rng_next(rng);
	float res =  number;
	res /= (float) 0x100000000ull;
	return res;
}

#endif //RANDOM_H
