CC=clang
CFLAGS = -std=c11 -Wall
CFLAGS += -g -lm -O2
CFLAGS += -Wno-unused-parameter -Wno-unused-function -Wno-double-promotion -Wno-missing-prototypes

batis: src/main.c src/vector.h
	$(CC) $(CFLAGS) -o batis $<

test.png: batis
	./batis test.png

.PHONY: clean test

clean:
	rm -f batis test.png

test: test.png
	feh test.png
